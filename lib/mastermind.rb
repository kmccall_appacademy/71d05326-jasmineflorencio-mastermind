class Code
  attr_reader :pegs
  PEGS = {
    "r" => "red",
    "o" => "orange",
    "y" => "yellow",
    "g" => "green",
    "b" => "blue",
    "p" => "purple"
  }
  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string)
    string.each_char do |letter|
      raise "Invalid color" unless PEGS.keys.include?(letter.downcase)
    end
    Code.new(string.chars)
  end


  def self.random
    Code.new(PEGS.keys.shuffle)
  end

  def [](index)
    pegs[index]
  end

  def exact_matches(code)
    matches = []
    code.pegs.each_with_index do |color, idx|
      matches << color if code.pegs[idx] == self.pegs[idx]
    end
    matches.length
  end

  def near_matches(code)
    code_hash = Hash.new(0)
    color_counter = 0
    code.pegs.each do |color|
      code_hash[color] += 1
    end
    code_hash.each do |color, count|
      if self.pegs.include?(color) && count > self.pegs.count(color)
        color_counter += (count - self.pegs.count(color))
      elsif self.pegs.include?(color) && count <= self.pegs.count(color)
        color_counter += count
      end
    end
    color_counter - exact_matches(code)
  end

  def ==(code)
    if code.class == Code
      code.pegs.join.downcase == self.pegs.join.downcase
    else
      false
    end
  end

end

class Game
  attr_reader :secret_code
  def initialize(secret_code= Code.random)
    @secret_code = secret_code
  end

  def get_guess
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(code)
    exact = @secret_code.exact_matches(code)
    near = @secret_code.near_matches(code)
    puts "You have #{exact} exact matches and #{near} near matches."
  end

end
